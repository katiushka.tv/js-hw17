"use strict"
const student = {
    name: prompt("Enter student's name"),
    lastName: prompt("Enter student's last name"),
    tabel: {},
}
console.log(student);

while (true) {
    let subject = prompt("Enter a subject");
    if (subject === null) {
        break;
    }
    student.tabel[subject] = +prompt("Enter student's grade");
}
console.log(student.tabel);

let badGrades = 0;
for (let key in student.tabel) {
    if (student.tabel[key] < 4) {
        badGrades++;
    }
}
if (badGrades === 0) {
    alert("Student has been transferred to the next course!");
}

let gradeSum = 0;
let quantity = 0;
for (let key in student.tabel) {
    quantity++;
    gradeSum += student.tabel[key];
    }
let averageGrade = gradeSum / quantity;
if (averageGrade > 7) {
    alert("Student has received a scholarship!");
}